Part A:
1, M: Hi Joe! I just saw your roommate coming out of the library. She looks different. Did she get her hair cut or something?
W: Yeah, three days ago actually. And hardly any one has noticed.
Q: What does the woman mean?

2, W: Haven’t you heard? The field trips’ve been called off because of the weather.
M: But Professor L told us just today to meet at four in front of the library, maybe I can still reach administrator’s office.
Q: What will the man probably do?

3, M: I'm sorry to hear about your car getting heating in the parking lot (停车场).Did you find out the other drivers with insurance or cover it? (这句还是觉得有的地方不对，没听出来)
W: Not yet, but I'm looking into it.
Q: What does the woman mean?

4, W: I think our baseball team's got a good chance of winning the championship this year.
M: What? Are you kidding? Have you seen them play recently?
Q: What does the man imply?

5, W: Are you sure this is the right way to the M auditorium? I can't afford to be late.
M: Oh, that won't happen; we'll be there in no time.
Q: What does the man mean?

6, W: John, I’d like to talk to you about the way you come late everyday, it disrupts the class.
M: I'm sorry professor, I didn't realize I was bothering anyone, I will watch from now on.
Q: What will the man probably do?

7, W: The drummer club is holding auditions for their annual(一年一度的) play, it's musical. What do you think?
B: To tell you the truth, I don't really act all thing, but thanks for thinking of me.
Q: What does the man mean?

8, M: What are you doing here? I thought you will be picking your brother up to the airport.
W: Oh, well, he called last night to say he has the flu.
Q: What does the woman imply?

9, W: I cannot believe my phone bill this month, it’s way too high.
M: Yeah, that happened to me a lot last year, but this year I have just tried to cut back on my long distance calls.
Q: What does the man imply?

10, W: What am I going to do, I have a big paper due Monday, But I promised Flora we'd clean the apartment this weekend.
M: Why don't you just see if Flora will postpone it for a week?
Q: What does the man suggest the woman do?

11, W: Didn't the comity say it would only take a month for them to complete the report？It took at least twice that long.
M: Yeah, there must with way off the mark (不相关)。
Q: What does the man mean?

12, M: There's a group of us going skiing this weekend, are you interested?
W: Not on your life, don't you remember that trip to Mountain Hope, when I went up to (前往) the cast for two month?
Q: Why does the woman imply?

13, M: Please be sure to put all the lab equipment back on the shelves when you are through with your experiment.
W: Oh, don't worry; I'm always very careful about that.
Q: What does the woman mean?

14, W: Thanks for picking up those grosses for me, how much do I owe you?
M: very even, you paid for the concert tickets last week, remember( however)?
Q: What does the man imply?

15, W: The soccer championship is run right now on channel 8.
M: If you don't mind, I'd like to see the end view of this nature program; it should only take another ten minutes itself.
Q: What will the man probably do?

16, M: Why did I ever agree to work on the school newspaper the same semester on taking 5 classes?
W: That's some lolled; you've got your hands full of right.
Q: What does the woman mean?

17, M: I'm trying to find someone to come with me to the new sculpture exhibit in the art museum on Saturday.
W: I'm not too crazy about art myself, but what about Mary? She’s taking an art history course and seems to be enjoying it.
Q: What does the woman suggest the man do?

18, W: You know, if you closed your windows all the way during the cold weather, your electric bills wouldn't be so high.
M: Sure, but my housemate will never agree to that.
Q: What does the man mean?

19, M: Gee, it keeps raining like this; they’ll probably cancel the play tonight.
W: Yeah, too bad they can't move to an indoor stage.
Q: What can be inferred from the conversation? 19，...can't just more

20, M: Hi Susan, if you are not doing anything for dinner tonight, a bunch of us are going to that new Italian restaurant in town.
W: Well, I am on the mood of some spaghetti and I have heard the food there is not bad.
Q: What does the woman imply?

21, M: There, the room looks really great now if we finally get the furniture, aren't you glad we made it to a clearance sale?
W: En, I still can't get over the huge discounts we got.
Q: What does the woman mean? ---清仓价

22, M: I've got to hurry home, my favorite television show comes on in a few minutes.
W: Don't you have anything better to do?
Q: What does the woman imply?

23, W: My car broke down and I've got to the doctor's appointment on the other side of town.
M: That's lucky you rang at me then, I can give you a ride on my way to work.
Q: What does the man offer to do?

24, M: This is the second time this month that my boss's asked me to work extra hours, I am glad to get a bigger paycheck, but I just don't want her to give me such a heavy schedule.
W: Better watch your step(谨慎).A lot of people would like to trade places with you.
Q: What does the woman imply the man should do? 

25, M: I don't know what I could have done to make Bob so mad at me.
W: Me neither, but it's likely to stay that way until you say you're sorry.
Q: What does the woman mean?

26, W: Hi Mike, I hear you and Peter down a new place.
M: Yeah, the apartment is pretty nice, but after living there a couple of months, I am wondering if I can afford to stay in it.
Q: What does the man imply?

27, A: This mirror is the perfect size for our bathroom, and it's half off.
B: We won't have time to hang it today, it'll still be here next week.
Q: What will the women probably do?

28, M: Please tell me I haven't missed the deadline for tuition payment.
W: I'm afraid you are about a week late.
Q: What does the woman mean?

29, M:I pretty much decide that I want to play on the soccer team next semester.
W: You are certainly good enough, but well, would you just tell me about how you really need to concentrate on improving your grades next semester?
Q: What does the woman imply?

30, W: I find Dr N's style of lecturing is so tedious.
M: Isn't it? And I've got a short of tension span.
Q: What does the man mean?

Part B:
31-35, listen to two students talking: 31-35,....sketches---素描画（这个应该是对的）
W: Hi, Mark, I didn't know you could paint.
M: Well, I'm just learning. it's for my studio art one of one class.
W: It's pretty good for a beginner.
M: Oh thanks, Judy. The instructor really liked my schedules, but she hasn't seen my painting yet.
W: En, there seems to be something wrong with it though.
M: Yeah, I know what you mean. It doesn't look like to me either.
W: I think I know, look here at the sky, it just seems to filled in with other colors of paining.
M: What do you mean? Everyone knows the sky is blue.
W: Well, that depends. Sometime it is and sometime it isn’t, as sunset can be full of reds and purples. Well, even now, look at now Mark, what do you see?
M: It looks blue to me.
W: Look again. do you see a kind of tallish color? 
M: Oh yeah, I see what you mean.
W: By adding some tan to your sky, I think you'll get more actual picture, and the color will look more natural.
M: I think I'll try that. Gee, how do you get to know so much about paining? Have you taken a lot of courses?
W: No, actually. But my father is an artist.
M: A professional artist?
W: Oh yeah. He shows his stophin lots of exhibits, everything is when we were kids; he always talks to us about his work.
M: I wish we could talk some more Judy. How about going for a cup of coffee? I’m ready for a break.
W: I'd love to. but I have to study for a history exam. In fact, I was just on my way to the study group, and I think I am already late. Maybe tomorrow?
M: Great, I'll meet you at the students’ center after my class. a little after three, OK?
W: Sounds good. Get around now.
M: Bye, Judy.
31, What are the speakers mainly discussing?
32, What is the man doing?
33, What does the woman suggest the man do?
34, How did the woman learn about painting?
35, What does the woman plan to do next?

36-39 listen to a conversation between two students. They are discussing material from US history class. 36-39,....stuff you missed...
pulled along by horses
M: Hey Teresa! thanks for agreeing to help me review all this history material.
W: No problem, Bob. So do you want to start with the stuff missed yesterday? They are part about urban problems in the colonial period?
M: Yeah, that'll be great.
W: Ok. Well, when the British were building cities in American colonies, they had to figure out how to make the cities run smoothly, right? Like...take traffic for instance.
M: Traffic? They had traffic that then?
W: Well, not the same kinds of traffic cities have today. Back in 18th centenary there were not any automobiles like there are now. But there were other vehicles like coaches and cars, you know, the kind had been pulled by horses say. Only few people could afford to have their own coaches then. but there were more and more public coaches. And cars, they were a big problem cause car driver usually overloaded their cars. And these oversize clumsy cars would slow down the traffic, and when you added to that all the farm animals.
M: Farm animals? In the cities?
W: Lots of them, cattle, pigs and other live stuck rolls in the streets. And they really caused trouble, although the pigs at least had good reasons for being their.
M: They did? 
W: Yeah, the town's people used pigs to clean up the garbage that was thrown into the streets. But then the pig interfered with traffic.
M: So what do the colonists do?
W: Well, they passed traffic laws. Like one that said, if your animals were found wondering in the street, they could be seized. Of course people who owned pigs didn't want to keep them penned up because the pigs were so good at cleaning up the streets. 
M: Sounds like a good idea letting them clean up the garbage. But I cannot see how pigs lying around in the streets could really slow down the traffic.

36, What is the main topic of the conversation?
37, According to the conversation, how did colonists try to make traveling in cities easier?
38, Why were cars consider the problem in colonial towns?
39, According to the conversation, what did colonial town's people do to clean their streets?

Part C
40-43, listen to part of the talk in a Geography class.
Not long ago, some of you may have read about the team of mountain climbing scientists who helped to recalculate the elevation of the highest mountain in the world, Mount Everest(珠穆朗玛峰). Of cause the elevation of Mount Everest was determined many years ago using traditional surveying methods. But these scientists wanted to make a more precise measurement, using a new method that takes advantage of recent advances in technology; it’s called the Global Positioning System. The Global Positioning System uses 24 satellites that circle the earth. Each of the satellites is constantly sending out signals, and each signal contains important information that can be used to determine the longitude, latitude and elevation at any point on the earth's surface. Well in order to use the system to calculate amount of Everest’s elevation. Scientists need to put a special receiver on the summit to receive signals from the satellites. The problem with this was that in the past, the receivers were much too heavy for climbers to carry. But now these receivers have been reduced to about the size and weight of a hand-held telephone, so climbers were able to take the receiver to the top of the Everest, and from there, to access the satellite system signals that would allow them to determine the precise elevation. And it turns out that the famous peak is actually a few feet higher than us previously thought.

40, What is the talk mainly about?
41, Why does the speaker mention a hand-held telephone?
42, According to the talk, what probably contributed most to the success of the new study of Mount Everest?
43,In the study described, how were satellite signals used?

听到28’42”,共35’24”


0209听抄1-43题sophie的0209听抄1-43题（感谢mimiming的初版）
Part A:
1, M: Hi Joe! I just saw your roommate coming out of the library. She looks different. Did she get her hair cut or something?
W: Yeah, three days ago actually. And hardly any one has noticed.
Q: What does the woman mean?

2, W: Haven’t you heard? The field trips’ve been called off because of the weather.
M: But Professor L told us just today to meet at four in front of the library, maybe I can still reach administrator’s office.
Q: What will the man probably do?

3, M: I''m sorry to hear about your car getting heating in the parking lot (停车场).Did you find out the other drivers with insurance or cover it? (这句还是觉得有的地方不对，没听出来)
W: Not yet, but I''m looking into it.
Q: What does the woman mean?第3题应该是INSURANCE，保险

4, W: I think our baseball team''s got a good chance of winning the championship this year.
M: What? Are you kidding? Have you seen them play recently?
Q: What does the man imply?

5, W: Are you sure this is the right way to the M auditorium? I can''t afford to be late.
M: Oh, that won''t happen; we''ll be there in no time.
Q: What does the man mean?

6, W: John, I’d like to talk to you about the way you come late everyday, it disrupts the class.
M: I''m sorry professor, I didn''t realize I was bothering anyone, I will watch from now on.
Q: What will the man probably do?

7, W: The drummer club is holding auditions for their annual(一年一度的) play, it''s musical. What do you think?
B: To tell you the truth, I don''t really act all thing, but thanks for thinking of me.
Q: What does the man mean?7，是AUDITION面视

8, M: What are you doing here? I thought you will be picking your brother up to the airport.
W: Oh, well, he called last night to say he has the flu.
Q: What does the woman imply?

9, W: I cannot believe my phone bill this month, it’s way too high.
M: Yeah, that happened to me a lot last year, but this year I have just tried to cut back on my long distance calls.
Q: What does the man imply?

10, W: What am I going to do, I have a big paper due Monday, But I promised Flora we''d clean the apartment this weekend.
M: Why don''t you just see if Flora will postpone it for a week?
Q: What does the man suggest the woman do?10缺IF，后面是WILL，不是WE‘LL
10题，Why don't you just see if Flora will postpone it for a week

11, W: Didn''t the comity say it would only take a month for them to complete the report？It took at least twice that long.
M: Yeah, there must with way off the mark (不相关)。
Q: What does the man mean?

12, M: There''s a group of us going skiing this weekend, are you interested?
W: Not on your life, don''t you remember that trip to Mountain Hope, when I went up to (前往) the cast for two month?
Q: Why does the woman imply?

13, M: Please be sure to put all the lab equipment back on the shelves when you are through with your experiment.
W: Oh, don''t worry; I''m always very careful about that.
Q: What does the woman mean?

14, W: Thanks for picking up those grosses for me, how much do I owe you?
M: very even, you paid for the concert tickets last week, remember?
Q: What does the man imply?
15, W: The soccer championship is run right now on channel 8.
M: If you don''t mind, I''d like to see the end view of this nature program; it should only take another ten minutes itself.
Q: What will the man probably do?

16, M: Why did I ever agree to work on the school newspaper the same semester on taking 5 classes?
W: That''s some lolled; you''ve got your hands full of right.
Q: What does the woman mean?

17, M: I''m trying to find someone to come with me to the new sculpture exhibit in the art museum on Saturday.
W: I''m not too crazy about art myself, but what about Mary? She’s taking an art history course and seems to be enjoying it.
Q: What does the woman suggest the man do?

18, W: You know, if you closed your windows all the way during the cold weather, your electric bills wouldn''t be so high.
M: Sure, but my housemate will never agree to that.
Q: What does the man mean?18题，应该是electric bill

19, M: Gee, it keeps raining like this; they’ll probably cancel the play tonight.
W: Yeah, too bad they can''t move to an indoor stage.
Q: What can be inferred from the conversation?19题，don't probarbly cancel the play tonight


20, M: Hi Susan, if you are not doing anything for dinner tonight, a bunch of us are going to that new Italian restaurant in town.
W: Well, I am on the mood of some spaghetti and I have heard the food there is not bad.
Q: What does the woman imply?20题，on the mood of some spaghetti

21, M: There, the room looks really great now if we finally get the furniture, aren''t you glad we made it to a clearest sale?
W: En, I still can''t get over the huge discounts we got.
Q: What does the woman mean?

22, M: I''''ve got to hurry home, my favorite television show comes on in a few minutes.
W: Don''t you have anything better to do?
Q: What does the woman imply?

23, W: My car broke down and I''ve got to the doctor''s appointment on the other side of town.
M: That''s lucky you rang at me then, I can give you a ride on my way to work.
Q: What does the man offer to do?

24, M: This is the second time this month that my boss''s asked me to work extra hours, I am glad to get a bigger paycheck, but I just don''t want her to give me such a heavy schedule.
W: Better watch your step(谨慎).A lot of people would like to trade places with you.
Q: What does the woman imply the man should do? 

25, M: I don''t know what I could have done to make Bob so mad at me.
W: Me neither, but it''s likely to stay that way until you say you''re sorry.
Q: What does the woman mean?

26, W: Hi Mike, I hear you and Peter down a new place.
M: Yeah, the apartment is pretty nice, but after living there a couple of months, I am wondering if I can afford to stay in it.
Q: What does the man imply?

27, A: This mirror is the perfect size for our bathroom, and it''s half off.
B: We won''t have time to hang it today, it''ll still be here next week.
Q: What will the women probably do?

28, M: Please tell me I haven''t missed the deadline for tuition payment.
W: I''m afraid you are about a week late.
Q: What does the woman mean?

29, M:I pretty much decide that I want to play on the soccer team next semester.
W: You are certainly good enough, but well, would you just tell me about how you really need to concentrate on improving your grades next semester?
Q: What does the woman imply?

30, W: I find Dr N''s style of lecturing is so tedious.
M: Isn''t it? And I''''ve got a short of tension span.
Q: What does the man mean?

Part B:
31-35, listen to two students talking:
W: Hi, Mark, I didn''t know you could paint.
M: Well, I''m just learning. it''s for my studio art one of one class.
W: It''''''''''''''''s pretty good for a beginner.
M: Oh thanks, Judy. The instructor really liked my schedules, but she hasn''t seen my painting yet.
W: En, there seems to be something wrong with it though.
M: Yeah, I know what you mean. It doesn''t look like to me either.
W: I think I know, look here at the sky, it just seems to filled in with other colors of paining.
M: What do you mean? Everyone knows the sky is blue.
W: Well, that depends. Sometime it is and sometime it isn’t, as sunset can be full of reds and purples. Well, even now, look at now Mark, what do you see?
M: It looks blue to me.
W: Look again. do you see a kind of tallish color? 
M: Oh yeah, I see what you mean.
W: By adding some tan to your sky, I think you''ll get more actual picture, and the color will look more natural.
M: I think I''ll try that. Gee, how do you get to know so much about paining? Have you taken a lot of courses?
W: No, actually. But my father is an artist.
M: A professional artist?
W: Oh yeah. He shows his stophin lots of exhibits, everything is when we were kids; he always talks to us about his work.
M: I wish we could talk some more Judy. How about going for a cup of coffee? I’m ready for a break.
W: I''d love to. but I have to study for a history exam. In fact, I was just on my way to the study group, and I think I am already late. Maybe tomorrow?
M: Great, I''ll meet you at the students’ center after my class. a little after three, OK?
W: Sounds good. Get around now.
M: Bye, Judy.
31, What are the speakers mainly discussing?
32, What is the man doing?
33, What does the woman suggest the man do?
34, How did the woman learn about painting?
35, What does the woman plan to do next?

36-39 listen to a conversation between two students. They are discussing material from US history class.
M: Hey Teresa! thanks for agreeing to help me review all this history material.
W: No problem, Bob. So do you want to start with the stuff missed yesterday? They are part about urban problems in the colonial period?
M: Yeah, that''ll be great.
W: Ok. Well, when the British were building cities in American colonies, they had to figure out how to make the cities run smoothly, right? Like...take traffic for instance.
M: Traffic? They had traffic that then?
W: Well, not the same kinds of traffic cities have today. Back in 18th centenary there were not any automobiles like there are now. But there were other vehicles like coaches and cars, you know, the kind had been pulled by horses say. Only few people could afford to have their own coaches then. but there were more and more public coaches. And cars, they were a big problem cause car driver usually overloaded their cars. And these oversize clumsy cars would slow down the traffic, and when you added to that all the farm animals.
M: Farm animals? In the cities?
W: Lots of them, cattle, pigs and other live stuck rolls in the streets. And they really caused trouble, although the pigs at least had good reasons for being their.
M: They did? 
W: Yeah, the town''''s people used pigs to clean up the garbage that was thrown into the streets. But then the pig interfered with traffic.
M: So what do the colonists do?
W: Well, they passed traffic laws. Like one that said, if your animals were found wondering in the street, they could be seized. Of course people who owned pigs didn''t want to keep them penned up because the pigs were so good at cleaning up the streets. 

M: Sounds like a good idea letting them clean up the garbage. But I cannot see how pigs lying around in the streets could really slow down the traffic.part b 第二段的最后一句话 应该是but i CAN see how.....吧好像cannot

36, What is the main topic of the conversation?
37, According to the conversation, how did colonists try to make traveling in cities easier?
38, Why were cars consider the problem in colonial towns?
39, According to the conversation, what did colonial town''''s people do to clean their streets?

Part C
40-43, listen to part of the talk in a Geography class.
Not long ago, some of you may have read about the team of mountain climbing scientists who helped to recalculate the elevation of the highest mountain in the world, Mount Everest(珠穆朗玛峰). Of cause the elevation of Mount Everest was determined many years ago using traditional "SURVEYING" methods. But these scientists wanted to make a more precise measurement, using a new method that takes advantage of recent advances in technology; it’s called the Global Positioning System. The Global Positioning System uses 24 satellites that"CIRCLE"the earth. Each of the satellites is constantly sending out signals, and each signal contains important information that can be used to determine the longitude, latitude and elevation at any point ON the earth''s surface. Well in order to use the system to calculate AMOUNT OF EVEREST''S elevation. Scientists need to put a special receiver on the summit to receive signals from the satellites. The problem with this was that in the past, the receivers were much too heavy for climbers to carry. But now these receivers have been reduced to about the size and weight of a hand-held telephone, so climbers were able to take the receiver to the top of the Everest, and from there, to access the satellite system signals that would allow them to determine the precise elevation. And it turns out that the famous peak is actually a few feet higher than us previously thought.

40, What is the talk mainly about?
41, Why does the speaker mention a hand-held telephone?
42, According to the talk, what probably contributed most to the success of the new study of Mount Everest?
43,In the study described, how were satellite signals used?

0209听力文字44-46
Human population near the equator have evolved dark skin over many generations 
because of exposure to the fiercest rays of the sun. A similar phenomenon has 
also occurred in other parts of the animal kingdom. The African grass mouse is 
a good example. Most mice are nocturnal, but the African grass mouse is active 
during daylight hours. This means that it spends its days searching for food in 
the semi-dry bush in squired habitats of eastern and southern Africa. Its furry 
stripe''''s like a chipmunk''''s, which helps it blend in with its environment. 

Because it spends a lot of time in the intense tropical sun, the grass mouse has 
also evolved two separate safeguards against the sun''''s ultraviolet radiation. 
First, like the population of humans in this region of the world, the skin of the 
grass mouse contains lots of melanin, or dark pigment. Second and quite unusual, 
this mouse has a layer of melanin pigmented tissue between its skull and skin. 
This unique cap provides an extra measure of protection for the grass mouse and 
three other types of African mouse, like rodents that are active during the day. 
The only other species scientists have identified with the same sort of skull of
that occasion is the white camp-making bat of the Central American tropic.
Although these bats sleep during the day, they do so curled up with their head
exposed to the sun.

44. What is the speaker mainly discussing?

45. What is unusual about the head of African grass-mouse?

46. Why do XXXX-making beds need protection from the sun''s rays?

